package br.com.jeanfbs.sqla;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;


@SpringBootApplication
public class MainApplication extends Application {

    private ConfigurableApplicationContext configurableApplicationContext;
    private Parent parent;

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void init() throws Exception {
        configurableApplicationContext= SpringApplication.run(MainApplication.class);
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/views/home.fxml"));
        fxmlLoader.setControllerFactory(configurableApplicationContext::getBean);
        parent= fxmlLoader.load();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        primaryStage.setTitle("SqlAutomate");
        Rectangle2D bounds = Screen.getPrimary().getBounds();
//        primaryStage.setScene(new Scene(parent, 600, 530));
        primaryStage.setScene(new Scene(parent, bounds.getWidth(), bounds.getHeight()));
        primaryStage.setResizable(false);
        primaryStage.show();

    }

    @Override
    public void stop() throws Exception {
        configurableApplicationContext.close();
    }
}
