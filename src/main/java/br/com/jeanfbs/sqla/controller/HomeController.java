package br.com.jeanfbs.sqla.controller;

import br.com.jeanfbs.sqla.context.ApplicationContext;
import br.com.jeanfbs.sqla.enums.ColumnType;
import br.com.jeanfbs.sqla.vo.Column;
import br.com.jeanfbs.sqla.vo.Table;
import com.microsoft.sqlserver.jdbc.SQLServerResultSet;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class HomeController {


    private static final Logger log = LoggerFactory.getLogger(HomeController.class);

    private static final Integer ICON_SIZE = 18;

    @FXML
    private TabPane navigationTabPane;

    @FXML
    private SplitPane splitPane;

    @FXML
    private MenuBar menuBar;

    @FXML
    private Tab structTab;


    @FXML
    private TabPane tableTabPane;

    @FXML
    private TreeView<String> tablesTree;


    private List<Table> tables;

    private Connection connection = ApplicationContext.getConnection();
    private Statement stmt;

    @FXML
    public void initialize() {

        useSystemMenuBar();

        ImageView structIcon = new ImageView(new Image(getClass()
                .getResourceAsStream("/assets/icons/tree-structure-80.png"),
                ICON_SIZE, ICON_SIZE, true, true));
        structIcon.setRotate(-180);
        structTab.setGraphic(structIcon);

        navigationTabPane.getSelectionModel().select(0);


        tables = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            Table table = new Table();
            table.setSchema("sqla");
            table.setName("tabela_" + i);

            List<Column> columns = new ArrayList<>();
            for (int j = 0; j < 10; j++) {
                Column col = new Column(String.format("coluna_%s%s", i , j), ColumnType.FLOAT, false);
                columns.add(col);
            }
            columns.get(0).setRequired(true);
            columns.get(1).setRequired(true);
            columns.get(2).setRequired(true);
            table.setColuns(Collections.unmodifiableList(columns));
            tables.add(table);
        }


        try {
            tablesTree.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
            populateTreeView(tables);
        } catch (SQLException e) {
            log.error("", e);
        }

        tablesTree.addEventHandler(MouseEvent.MOUSE_CLICKED, this::handleClickTableItem);

//        try {
//            this.stmt = connection.createStatement();
//            tables = selectAllTables();
//            tables.forEach(System.out::println);
//        } catch (SQLException e) {
//            log.error("", e);
//        }
    }

    private void useSystemMenuBar() {

        final String os = System.getProperty("os.name");
        if (os != null && os.startsWith("Mac")){
            menuBar.useSystemMenuBarProperty().set(true);
            AnchorPane.setTopAnchor(splitPane, 0.0);
        }else{
            AnchorPane.setTopAnchor(splitPane, 30.0);
        }
    }


    private void handleClickTableItem(MouseEvent ev){

        if(ev.getClickCount() == 2){

            Optional<TreeItem<String>> treeItemOpt = tablesTree.getSelectionModel().getSelectedItems().stream().findFirst();
            if(treeItemOpt.isPresent()){
                String tableName = treeItemOpt.get().getValue();
                Optional<Table> tableOpt = tables.stream().filter(table -> table.getName().equals(tableName)).findFirst();

                if(tableOpt.isPresent()){

                    Tab tab = new Tab();
                    ImageView tableIcon = new ImageView(new Image(getClass()
                            .getResourceAsStream("/assets/icons/data-grid-80.png"),
                            ICON_SIZE, ICON_SIZE, true, true));
                    tab.setGraphic(tableIcon);
                    tab.setText(tableName);
                    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/views/tabs/tableTab.fxml"));

                    try {

                        Parent tabNode = fxmlLoader.load();
                        tabNode.setUserData(tableOpt.get());
                        tab.setContent(tabNode);
                        tableTabPane.getTabs().add(tab);
                    } catch (IOException e) {
                        log.error("",e);
                    }
                }
            }
        }

    }

    private void populateTreeView(List<Table> tables) throws SQLException {

        ImageView databaseIcon = new ImageView(new Image(getClass()
                .getResourceAsStream("/assets/icons/database-80.png"),
                ICON_SIZE, ICON_SIZE, true, true));

        TreeItem<String> rootItem = new TreeItem<> ("Sqla", databaseIcon);
//        TreeItem<String> rootItem = new TreeItem<> (connection.getCatalog(), databaseIcon);
        rootItem.setExpanded(true);
        List<TreeItem<String>> treeItems = tables.stream()
                .map(this::createTableItem).collect(Collectors.toList());
        rootItem.getChildren().addAll(treeItems);
        tablesTree.setRoot(rootItem);
    }

    private TreeItem<String> createTableItem(Table table) {

        ImageView tableIcon = new ImageView(new Image(getClass()
                .getResourceAsStream("/assets/icons/data-grid-80.png"),
                ICON_SIZE, ICON_SIZE, true, true));

        return new TreeItem<>(table.getName(), tableIcon);
    }

    private List<String> selectAllTables() throws SQLException {

        List<String> tables = new ArrayList<>();

        SQLServerResultSet resultSet = (SQLServerResultSet) this.stmt
                .executeQuery("SELECT table_catalog, table_schema, table_name, table_type " +
                        "FROM information_schema.tables WHERE table_catalog = 'sqla'");

        while(resultSet.next()){
            String tableName = resultSet.getString("table_name");
            tables.add(tableName);
        }

        return tables;
    }


}
